package com.esser.marcelo.things.vvm.activity.main

import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.MutableLiveData
import com.esser.marcelo.things.dao.EnderecoDAO
import com.esser.marcelo.things.model.EnderecoDTO
import com.esser.marcelo.things.service.network.RetrofitInitializer
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Marcelo Esser
 * @since 31/12/18
 */
class MainActivityViewModel : LifecycleObserver {

    private val call = RetrofitInitializer().viacepService()

    var endereco: MutableLiveData<EnderecoDTO> = MutableLiveData()

    private val dao = EnderecoDAO()
    var listaEnderecos: List<EnderecoDTO> = dao.enderecos

    fun buscaEndereco(
        cep: String,
        onEnderecoEncontrado: (endereco: EnderecoDTO) -> Unit,
        //HOF - HIGHER ORDER FUNCTION, função passada por parâmetro
        onFailure: (errorMessage: String) -> Unit) {
        call.pegaEndereco(cep).enqueue(object : Callback<EnderecoDTO> {
            override fun onFailure(call: Call<EnderecoDTO>, t: Throwable) {
                //definindo o valor do parâmetro da HOF
                onFailure(t.message.toString())
            }

            override fun onResponse(call: Call<EnderecoDTO>, response: Response<EnderecoDTO>) {
                if (response.body() != null) {
                    //Atribuindo valor ao objeto, utilizando live data
                    endereco.value = response.body()
                    onEnderecoEncontrado(endereco.value as EnderecoDTO)

                } else {
                    onFailure("Cep inválido")
                }
            }
        })
    }

    fun salvaEndereco(endereco: EnderecoDTO) {
        dao.adiciona(endereco)
    }

    fun alteraEndereco(endereco: EnderecoDTO, posicao: Int) {
        dao.altera(endereco, posicao)
    }

    fun removeEndereco(posicao: Int) {
        dao.remove(posicao)
    }
}