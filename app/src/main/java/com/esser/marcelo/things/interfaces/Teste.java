package com.esser.marcelo.things.interfaces;

import kotlin.Unit;

/**
 * @author Marcelo Esser
 * @since 29/01/19
 */
public interface Teste {
    void onTeste();
}
