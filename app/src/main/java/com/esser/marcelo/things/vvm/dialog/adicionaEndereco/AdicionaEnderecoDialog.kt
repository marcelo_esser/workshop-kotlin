package com.esser.marcelo.things.vvm.dialog.adicionaEndereco

import android.content.Context
import android.view.ViewGroup
import com.esser.marcelo.things.R
import com.esser.marcelo.things.vvm.dialog.FormEnderecoDialog

/**
 * @author Marcelo Esser
 * @since 03/01/19
 */
class AdicionaEnderecoDialog(
    viewGroup: ViewGroup,
    context: Context) : FormEnderecoDialog(context, viewGroup) {

    override val tituloBotao: String
        get() = "Adicionar"

    override fun titulo(): Int {
        return R.string.adiciona_endereco
    }

}