package com.esser.marcelo.things.service.network

import com.esser.marcelo.things.service.viacep.IViacepAPI
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * @author Marcelo Esser
 * @since 01/01/19
 */
class RetrofitInitializer {

    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl("https://viacep.com.br/ws/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun viacepService(): IViacepAPI = retrofit.create(IViacepAPI::class.java)
}