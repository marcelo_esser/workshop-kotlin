package com.esser.marcelo.things.service.viacep

import com.esser.marcelo.things.model.EnderecoDTO
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * @author Marcelo Esser
 * @since 31/12/18
 */
interface IViacepAPI {

    @GET("{cep}/json")
    fun pegaEndereco(@Path("cep") cep: String): Call<EnderecoDTO>
}