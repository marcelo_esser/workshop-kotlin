package com.esser.marcelo.things.delegate

import com.esser.marcelo.things.model.EnderecoDTO

/**
 * @author Marcelo Esser
 * @since 04/01/19
 */
interface EnderecoDelegate {

    fun enderecoClickListener(endereco: EnderecoDTO, posicao: Int)

    fun enderecoLongClickListener(endereco: EnderecoDTO, posicao: Int)
}