package com.esser.marcelo.things.vvm.activity.main

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.ViewGroup
import android.widget.Toast
import com.esser.marcelo.things.R
import com.esser.marcelo.things.adapter.EnderecosAdapter
import com.esser.marcelo.things.delegate.EnderecoDelegate
import com.esser.marcelo.things.model.EnderecoDTO
import com.esser.marcelo.things.vvm.dialog.AlteraEnderecoDialog.AlteraEnderecoDialog
import com.esser.marcelo.things.vvm.dialog.adicionaEndereco.AdicionaEnderecoDialog
import com.esser.marcelo.things.vvm.dialog.deletaEnderecoDialog.DeletaEnderecoDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), EnderecoDelegate {

    private lateinit var viewModel: MainActivityViewModel

    private val enderecosAdapter: EnderecosAdapter by lazy {
        EnderecosAdapter(this, viewModel.listaEnderecos, this)
    }

    private val viewGroupDaActivity: ViewGroup by lazy {
        window.decorView as ViewGroup
    }

    private var stringNull: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = MainActivityViewModel()

        adicionaObserverNoEndereco()

        configuraLista()

        btnBuscaCep.setOnClickListener {
            buscaEndereco()
        }
        stringNull = "Hello World"

        val qtdCaracteres: Int = stringNull?.length ?:0

        Toast.makeText(this, "${"A quantidade de caracteres é de " + stringNull}: ${qtdCaracteres}", Toast.LENGTH_SHORT)
            .show()
    }

    private fun buscaEndereco() {
        viewModel.buscaEndereco(
            etCep.text.toString(),
            //Utilizando a HOF, para fazer um callback, chamando o método onError onde mostrará uma toast com o erro
            //vindo por parâmetro
            onFailure = { errorMessage ->
                onError(errorMessage)

            },
            onEnderecoEncontrado = { enderecoEncontrado ->
                chamaAdicionaEndereco(enderecoEncontrado)
            })
    }

    private fun chamaAdicionaEndereco(enderecoEncontrado: EnderecoDTO) {
        AdicionaEnderecoDialog(viewGroupDaActivity, this)
            .chama(endereco = enderecoEncontrado,
                delegate = { enderecoCriado ->
                    enderecoCriado.cep = etCep.getText().toString()
                    adicionaEndereco(enderecoCriado)
                })
    }

    private fun onError(errorMessage: String) {
        Toast.makeText(this@MainActivity, errorMessage, Toast.LENGTH_LONG).show()
    }

    private fun adicionaObserverNoEndereco() {
        viewModel.endereco.observe(this, Observer { enderecoRetornado: EnderecoDTO? ->
            if (enderecoRetornado != null) {
                preencheHeader(enderecoRetornado)
            }
        })
    }

    private fun preencheHeader(enderecoRetornado: EnderecoDTO) {
        tvCEP.text = enderecoRetornado.cep
        tvLogradouro.text = enderecoRetornado.logradouro
        tvBairro.text = enderecoRetornado.bairro
    }


    private fun chamaAlteraEndereco(endereco: EnderecoDTO, posicao: Int) {
        AlteraEnderecoDialog(viewGroupDaActivity, this)
            .chama(endereco = endereco,
                delegate = { enderecoAlterado ->
                    alteraEndereco(enderecoAlterado, posicao)
                })
    }

    private fun configuraLista() {
        with(rvEnderecos) {
            adapter = enderecosAdapter
        }
    }

    override fun enderecoClickListener(endereco: EnderecoDTO, posicao: Int) {
        chamaAlteraEndereco(endereco, posicao)
    }

    override fun enderecoLongClickListener(endereco: EnderecoDTO, posicao: Int) {
        chamaRemoveEndereco(posicao)
    }

    private fun chamaRemoveEndereco(posicao: Int) {
        DeletaEnderecoDialog(this).configuraDialog(
            deletaEndereco = {
                removeEndereco(posicao)
            })
    }

    private fun removeEndereco(posicao: Int) {
        viewModel.removeEndereco(posicao)
        configuraLista()
    }

    private fun adicionaEndereco(enderecoCriado: EnderecoDTO) {
        viewModel.salvaEndereco(enderecoCriado)
        configuraLista()
    }

    private fun alteraEndereco(enderecoAlterado: EnderecoDTO, posicao: Int) {
        viewModel.alteraEndereco(enderecoAlterado, posicao)
        configuraLista()
    }
}

