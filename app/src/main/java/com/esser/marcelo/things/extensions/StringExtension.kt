package com.esser.marcelo.things.extensions

/**
 * @author Marcelo Esser
 * @since 04/02/19
 */
fun String.formataCEP(): String {
    val firstaPart: String = this.substring(0, this.length - 3)
    val secondPart: String = this.substring(this.length - 3, this.length)

    return "${firstaPart} - ${secondPart}"
}