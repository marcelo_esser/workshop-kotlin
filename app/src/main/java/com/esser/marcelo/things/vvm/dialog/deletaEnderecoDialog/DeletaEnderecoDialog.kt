package com.esser.marcelo.things.vvm.dialog.deletaEnderecoDialog

import android.content.Context
import android.support.v7.app.AlertDialog
import com.esser.marcelo.things.R

/**
 * @author Marcelo Esser
 * @since 04/01/19
 */
class DeletaEnderecoDialog(
    private val context: Context
) {

    fun configuraDialog(deletaEndereco: () -> Unit) {
        AlertDialog.Builder(context)
            .setTitle(R.string.titulo_dialog_deleta)
            .setMessage(R.string.dialog_deleta_mensagem)
            .setPositiveButton("Sim") { dialogInterface, i ->
                deletaEndereco()
            }
            .setNegativeButton("Não", null)
            .show()
    }
}