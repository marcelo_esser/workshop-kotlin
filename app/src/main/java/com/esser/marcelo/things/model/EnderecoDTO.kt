package com.esser.marcelo.things.model

import com.google.gson.annotations.SerializedName

/**
 * @author Marcelo Esser
 * @since 31/12/18
 */
class EnderecoDTO(
    @SerializedName("cep")
    var cep: String? = null,
    @SerializedName("logradouro")
    val logradouro: String = "logradouro",
    @SerializedName("complemento")
    val complemento: String = "complemento",
    @SerializedName("bairro")
    val bairro: String = "bairro",
    @SerializedName("localidade")
    val localidade: String = "localidade",
    @SerializedName("uf")
    val uf: String = "unidade federal",
    @SerializedName("unidade")
    val unidade: String = "unidade",
    @SerializedName("ibge")
    val ibge: String = "ibge",
    @SerializedName("gia")
    val gia: String = "gia"
)