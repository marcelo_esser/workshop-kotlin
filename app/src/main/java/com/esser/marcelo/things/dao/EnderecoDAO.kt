package com.esser.marcelo.things.dao

import com.esser.marcelo.things.model.EnderecoDTO

/**
 * @author Marcelo Esser
 * @since 03/01/19
 */
class EnderecoDAO {

    val enderecos: List<EnderecoDTO> = _enderecos

    companion object {
        private val _enderecos: MutableList<EnderecoDTO> = mutableListOf()
    }

    fun adiciona(endereco: EnderecoDTO) = _enderecos.add(endereco)

    fun altera(endereco: EnderecoDTO, posicao: Int){
        _enderecos[posicao] = endereco
    }

    fun remove(posicao: Int){
        _enderecos.removeAt(posicao)
    }
}