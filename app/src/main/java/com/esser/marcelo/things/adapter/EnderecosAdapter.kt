package com.esser.marcelo.things.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.esser.marcelo.things.R
import com.esser.marcelo.things.delegate.EnderecoDelegate
import com.esser.marcelo.things.extensions.formataCEP
import com.esser.marcelo.things.model.EnderecoDTO
import kotlinx.android.synthetic.main.row_enderecos.view.*

/**
 * @author Marcelo Esser
 * @since 31/12/18
 */

class EnderecosAdapter(
    private val context: Context,
    private val enderecos: List<EnderecoDTO>,
    private val delegate: EnderecoDelegate
) : RecyclerView.Adapter<EnderecosAdapter.ViewHolder>() {

    override fun onBindViewHolder(viewHolder: ViewHolder, posicao: Int) {
        val endereco = enderecos[posicao]
        preencheCampos(endereco, viewHolder)

        viewHolder.itemView.setOnClickListener {
            delegate.enderecoClickListener(endereco, posicao)
        }

        viewHolder.itemView.setOnLongClickListener {
            delegate.enderecoLongClickListener(endereco, posicao)
            true
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, view: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.row_enderecos, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = enderecos.size

    private fun preencheCampos(endereco: EnderecoDTO, viewHolder: ViewHolder) {
        with(viewHolder) {

            endereco.cep?.let { cepNaoNulo ->
                tvCep.text = cepNaoNulo.formataCEP()
            }

            tvCep.text = endereco.cep?.formataCEP()
            tvBairro.text = endereco.bairro
            tvLogradouro.text = endereco.logradouro
            tvUf.text = endereco.uf
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvCep: TextView = itemView.tvCEP
        val tvBairro = itemView.tvBairro
        val tvLogradouro = itemView.tvLogradouro
        val tvUf = itemView.tvUf
    }
}