package com.esser.marcelo.things.vvm.dialog.AlteraEnderecoDialog

import android.content.Context
import android.view.ViewGroup
import com.esser.marcelo.things.R
import com.esser.marcelo.things.vvm.dialog.FormEnderecoDialog

/**
 * @author Marcelo Esser
 * @since 03/01/19
 */
class AlteraEnderecoDialog (
    viewGroup: ViewGroup,
    context: Context
) : FormEnderecoDialog(context, viewGroup) {

    //Variável extendida a partir de FormEnderecoDialog
    override val tituloBotao: String
        get() = "Alterar"


    //Função extendida a partir de FormEnderecoDialog
    override fun titulo(): Int {
        return R.string.altera_endereco
    }

}