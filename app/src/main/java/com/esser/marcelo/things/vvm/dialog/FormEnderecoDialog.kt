package com.esser.marcelo.things.vvm.dialog

import android.content.Context
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.esser.marcelo.things.R
import com.esser.marcelo.things.model.EnderecoDTO
import kotlinx.android.synthetic.main.dialog_endereco.view.*

/**
 * @author Marcelo Esser
 * @since 03/01/19
 */
abstract class FormEnderecoDialog(
//properties criadas a partir de parâmetros via construtor,
// para uso no decorrer do código
    private val context: Context,
    private val viewGroup: ViewGroup
) {

    //variáveis criadas nesta classe para usar nesta classe
    private val viewCriada = criaLayout()

    //variável abstrata, a qual vai definir o titulo do botão a partir das classes filhas
    abstract protected val tituloBotao: String

    //métdo o qual chama a dialog, executando suas funções descendentes.
    //É passada uma HOF como parâmetropara que possas ser usada na "constroiDialog"
    fun chama(endereco: EnderecoDTO, delegate: (endereco: EnderecoDTO) -> Unit) {
        constroiDialog(delegate)
        preencheFormulario(endereco)
    }

    //Método o qual constrói a dialog
    private fun constroiDialog(delegate: (endereco: EnderecoDTO) -> Unit) {
        AlertDialog.Builder(context)
            .setTitle(titulo())
            .setView(viewCriada)
            .setPositiveButton(tituloBotao) { dialogInterface, i ->
                //Adicionamos o enderecoCriado() ao parâmetro da hof,
                // para que possa ser usado nas chamadas do
                // constroiDialog()
                delegate(enderecoCriado())
            }
            .setNegativeButton("Cancelar", null)
            .show()
    }

    //Endereço criado a partir dos campos da dialog
    private fun enderecoCriado(): EnderecoDTO {
        val enderecoCriado = EnderecoDTO(
            uf = viewCriada.etUf.text.toString(),
            localidade = viewCriada.etCidade.text.toString(),
            bairro = viewCriada.etBairro.text.toString(),
            logradouro = viewCriada.etRua.text.toString()
        )
        return enderecoCriado
    }

    //Método o qual infla o layout que será usado na dialog
    private fun criaLayout(): View {
        return LayoutInflater.from(context).inflate(
            R.layout.dialog_endereco,
            viewGroup, false
        )
    }

    //Método abstrato que retornará um resource (R.string.*) contendo a string com o titulo que sera usado na dialog
    abstract fun titulo(): Int

    //método que preencherá o formulário
    private fun preencheFormulario(endereco: EnderecoDTO) {
        viewCriada.etUf.setText(endereco.uf)
        viewCriada.etCidade.setText(endereco.localidade)
        viewCriada.etBairro.setText(endereco.bairro)
        viewCriada.etRua.setText(endereco.logradouro)
    }
}